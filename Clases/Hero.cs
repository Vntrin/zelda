﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.Clases;
using System.Windows.Forms;

namespace WindowsFormsApp4.Clases
{
    class Hero
    {
        int orientacion;
        Arma equipada;
        bool equip;

        public Hero(int orientacion, bool equip)
        {
            this.orientacion = orientacion;
            equipada = null;
            this.equip = false;
        }


        internal Arma Equipada { get => equipada; set => equipada = value; }
        public int Orientacion { get => orientacion; set => orientacion = value; }

        public void moverLink(object sender, KeyEventArgs e, PictureBox link)
        {


            if (e.KeyCode == (Keys.D))
            {
                Orientacion = 4;
                link.Left += 30;

            }

            else if (e.KeyCode == Keys.A)
            {
                Orientacion = 2;
                link.Left -= 30;

            }

            else if (e.KeyCode == Keys.W)
            {
                Orientacion = 3;
                link.Top -= 30;

            }

            else if (e.KeyCode == Keys.S)
            {
                Orientacion = 1;
                link.Top += 30;

            }
        }
    
      
        
        public void Atacar(Enemigo elEnemigo, PictureBox enemy, PictureBox mono, Hero link, object sender, KeyEventArgs e)
        {
            if (equip == false)
            {
                if (enemy.Bounds.IntersectsWith(mono.Bounds))
                {
                    elEnemigo.colision(link, mono, sender, e);
                }
            }
            else if (equip == true)
            {
                if (enemy.Bounds.IntersectsWith(mono.Bounds))
                {
                    elEnemigo.BajarVida(Equipada.Danio, mono, enemy);
                    elEnemigo.colision(link, mono, sender, e);
                    
                }
            }
        }
        public void EquiparArma(Arma equipada, Hero link, PictureBox mono, PictureBox arma, Label label )
        {
            if (mono.Bounds.IntersectsWith(arma.Bounds))
            {
                arma.Visible = false;
                equip = true;
            }

            if (equip == true)
            {
                Equipada = equipada; 
                label.Text = "Rompe caras >:v";
            } else
            {
                label.Text = "No haces nada";
            } 
          
        }  
    }
}
