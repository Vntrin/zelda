﻿namespace WindowsFormsApp4
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.obstaculo3 = new System.Windows.Forms.PictureBox();
            this.Espada = new System.Windows.Forms.PictureBox();
            this.obstaculo2 = new System.Windows.Forms.PictureBox();
            this.malo1 = new System.Windows.Forms.PictureBox();
            this.obstaculo4 = new System.Windows.Forms.PictureBox();
            this.obstaculo5 = new System.Windows.Forms.PictureBox();
            this.obstaculo1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.malo2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.malo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.malo2)).BeginInit();
            this.SuspendLayout();
            // 
            // obstaculo3
            // 
            this.obstaculo3.BackColor = System.Drawing.Color.Red;
            this.obstaculo3.Location = new System.Drawing.Point(90, 270);
            this.obstaculo3.Name = "obstaculo3";
            this.obstaculo3.Size = new System.Drawing.Size(30, 30);
            this.obstaculo3.TabIndex = 1;
            this.obstaculo3.TabStop = false;
            this.obstaculo3.Click += new System.EventHandler(this.obstaculo3_Click);
            // 
            // Espada
            // 
            this.Espada.Image = global::WindowsFormsApp4.Properties.Resources.Espada;
            this.Espada.Location = new System.Drawing.Point(300, 180);
            this.Espada.Name = "Espada";
            this.Espada.Size = new System.Drawing.Size(20, 30);
            this.Espada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Espada.TabIndex = 1;
            this.Espada.TabStop = false;
            this.Espada.VisibleChanged += new System.EventHandler(this.pictureBox7_VisibleChanged);
            // 
            // obstaculo2
            // 
            this.obstaculo2.BackColor = System.Drawing.SystemColors.MenuText;
            this.obstaculo2.Location = new System.Drawing.Point(300, 270);
            this.obstaculo2.Name = "obstaculo2";
            this.obstaculo2.Size = new System.Drawing.Size(30, 30);
            this.obstaculo2.TabIndex = 1;
            this.obstaculo2.TabStop = false;
            this.obstaculo2.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // malo1
            // 
            this.malo1.Image = global::WindowsFormsApp4.Properties.Resources._22554282_1474321255937973_282813284_n;
            this.malo1.Location = new System.Drawing.Point(421, 270);
            this.malo1.Name = "malo1";
            this.malo1.Size = new System.Drawing.Size(30, 30);
            this.malo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.malo1.TabIndex = 1;
            this.malo1.TabStop = false;
            this.malo1.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // obstaculo4
            // 
            this.obstaculo4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.obstaculo4.Location = new System.Drawing.Point(60, 60);
            this.obstaculo4.Name = "obstaculo4";
            this.obstaculo4.Size = new System.Drawing.Size(30, 30);
            this.obstaculo4.TabIndex = 1;
            this.obstaculo4.TabStop = false;
            // 
            // obstaculo5
            // 
            this.obstaculo5.BackColor = System.Drawing.SystemColors.MenuText;
            this.obstaculo5.Location = new System.Drawing.Point(420, 60);
            this.obstaculo5.Name = "obstaculo5";
            this.obstaculo5.Size = new System.Drawing.Size(30, 30);
            this.obstaculo5.TabIndex = 1;
            this.obstaculo5.TabStop = false;
            this.obstaculo5.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // obstaculo1
            // 
            this.obstaculo1.BackColor = System.Drawing.SystemColors.GrayText;
            this.obstaculo1.Location = new System.Drawing.Point(390, 150);
            this.obstaculo1.Name = "obstaculo1";
            this.obstaculo1.Size = new System.Drawing.Size(30, 30);
            this.obstaculo1.TabIndex = 1;
            this.obstaculo1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WindowsFormsApp4.Properties.Resources._4AmWDfR;
            this.pictureBox1.Location = new System.Drawing.Point(210, 180);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.LocationChanged += new System.EventHandler(this.pictureBox1_LocationChanged);
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // malo2
            // 
            this.malo2.Image = global::WindowsFormsApp4.Properties.Resources._22554282_1474321255937973_282813284_n;
            this.malo2.Location = new System.Drawing.Point(90, 150);
            this.malo2.Name = "malo2";
            this.malo2.Size = new System.Drawing.Size(30, 30);
            this.malo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.malo2.TabIndex = 1;
            this.malo2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(508, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(260, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 479);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.obstaculo3);
            this.Controls.Add(this.Espada);
            this.Controls.Add(this.obstaculo2);
            this.Controls.Add(this.malo2);
            this.Controls.Add(this.malo1);
            this.Controls.Add(this.obstaculo4);
            this.Controls.Add(this.obstaculo5);
            this.Controls.Add(this.obstaculo1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.malo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obstaculo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.malo2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox obstaculo1;
        private System.Windows.Forms.PictureBox obstaculo5;
        private System.Windows.Forms.PictureBox obstaculo4;
        private System.Windows.Forms.PictureBox malo1;
        private System.Windows.Forms.PictureBox obstaculo2;
        private System.Windows.Forms.PictureBox Espada;
        private System.Windows.Forms.PictureBox obstaculo3;
        private System.Windows.Forms.PictureBox malo2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

